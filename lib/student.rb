

class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(course)
    self.courses.each do |course1|
      if course1.conflicts_with?(course)
        raise "conflicting class schedule"
      end
    end
    unless @courses.include?(course)
      self.courses << course
      course.students << self
    end

  end

  def course_load
    hash = Hash.new(0)
    self.courses.each do |course|
      hash[course.department] += course.credits
    end
    hash
  end



  attr_accessor :first_name, :last_name, :courses
end
